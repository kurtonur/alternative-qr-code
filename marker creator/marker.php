<?php

header('Content-type: image/svg+xml');
error_reporting(0);

$R=$_GET["r"] ? $_GET["r"] : 0;
$G=$_GET["g"] ? $_GET["g"] : 0;
$B=$_GET["b"] ? $_GET["b"] : 0;
$getCode=$_GET["c"] ? $_GET["c"] : rand(0,100000);
$Size=$_GET["s"] ? $_GET["s"] : 1;

$times=$Size;

$canvasX=430*$times;
$canvasY=$canvasX;



?>
<svg width="<?php echo $canvasX; ?>" height="<?php echo $canvasY; ?>" viewBox="0 0 <?php echo $canvasX.' '.$canvasY; ?>" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" >
   

<?php


echo '<circle cx="'.($canvasX/2).'" cy="'.($canvasY/2).'" r="'.(208*$times).'" style="fill:rgb('.$R.','.$B.','.$G.');" />';
echo '<circle cx="'.($canvasX/2).'" cy="'.($canvasY/2).'" r="'.(200*$times).'" style="fill:rgb(255,255,255);" />';
echo '<circle cx="'.($canvasX/2).'" cy="'.($canvasY/2).'" r="'.(113*$times).'" style="fill:rgb('.$R.','.$B.','.$G.');" />';
echo '<circle cx="'.($canvasX/2).'" cy="'.($canvasY/2).'" r="'.(83*$times).'" style="fill:rgb(255,255,255);" />';


$pointsb=Daire(100*$times,$canvasY/2,90);
for($i=0;$i<4;$i++){
	if($i!=1)
		echo '<circle cx="'.($pointsb[0][$i]).'" cy="'.($pointsb[1][$i]).'" r="'.(9*$times).'" style="fill:rgb(255,255,255);" />';
}
/*
for($i=0;$i<4;$i++){
	if($i!=1)
		echo '<circle cx="'.($pointsb[0][$i]).'" cy="'.($pointsb[1][$i]).'" r="'.(4*$times).'" style="fill:rgb('.$R.','.$B.','.$G.');" />';
}*/

$Code = ArCode($getCode);

$CodeCount=0;
$points2=Daire(180*$times,$canvasY/2,15);
$points3=Daire(140*$times,$canvasY/2,18);


for($i=0;$i<20;$i++){
	
	if(strlen($Code)>$CodeCount){
		if($Code[$CodeCount]==1)
			echo '<circle cx="'.($points3[0][$i]).'" cy="'.($points3[1][$i]).'" r="'.(13*$times).'" style="fill:rgb('.$R.','.$B.','.$G.');" />';
		//else echo '<rect x="'.($points3[0][$i]-(14*$times)).'" y="'.($points3[1][$i]-(14*$times)).'" width="'.(28*$times).'" height="'.(28*$times).'" style="fill:rgb('.$R.','.$B.','.$G.');"  />';
		//else echo '<text x="'.($points3[0][$i]-(18*$times)).'" y="'.($points3[1][$i]+(18*$times)).'" style="fill:rgb('.$R.','.$B.','.$G.');font-size:'.(50*$times).'px;" >✦</text>';
		$CodeCount++;
	}
	else {
		if(rand (0,1)==1)
			echo '<circle cx="'.($points3[0][$i]).'" cy="'.($points3[1][$i]).'" r="'.(13*$times).'" style="fill:rgb('.$R.','.$B.','.$G.');" />';
		//else echo '<rect x="'.($points3[0][$i]-(14*$times)).'" y="'.($points3[1][$i]-(14*$times)).'" width="'.(28*$times).'" height="'.(28*$times).'" style="fill:rgb('.$R.','.$B.','.$G.');"  />';
		//else echo '<text x="'.($points3[0][$i]-(18*$times)).'" y="'.($points3[1][$i]+(18*$times)).'" style="fill:rgb('.$R.','.$B.','.$G.');font-size:'.(50*$times).'px;" >✦</text>';
	}
	
}

for($i=0;$i<24;$i++){
	
	if(strlen($Code)>$CodeCount){
		if($Code[$CodeCount]==1)
			echo '<circle cx="'.($points2[0][$i]).'" cy="'.($points2[1][$i]).'" r="'.(13*$times).'" style="fill:rgb('.$R.','.$B.','.$G.');" />';
		//else echo '<rect x="'.($points2[0][$i]-(14*$times)).'" y="'.($points2[1][$i]-(14*$times)).'" width="'.(28*$times).'" height="'.(28*$times).'" style="fill:rgb('.$R.','.$B.','.$G.');"  />';
		//else echo '<text x="'.($points2[0][$i]-(18*$times)).'" y="'.($points2[1][$i]+(18*$times)).'" style="fill:rgb('.$R.','.$B.','.$G.');font-size:'.(50*$times).'px;" >✦</text>';
		$CodeCount++;
	}
	else {
		if(rand (0,1)==1)
			echo '<circle cx="'.($points2[0][$i]).'" cy="'.($points2[1][$i]).'" r="'.(13*$times).'" style="fill:rgb('.$R.','.$B.','.$G.');" />';
		//else echo '<rect x="'.($points2[0][$i]-(14*$times)).'" y="'.($points2[1][$i]-(14*$times)).'" width="'.(28*$times).'" height="'.(28*$times).'" style="fill:rgb('.$R.','.$B.','.$G.');"  />';
		//else echo '<text x="'.($points2[0][$i]-(18*$times)).'" y="'.($points2[1][$i]+(18*$times)).'" style="fill:rgb('.$R.','.$B.','.$G.');font-size:'.(50*$times).'px;" >✦</text>';
	}
}


?>
</svg>

<?php

function Daire($r,$merkez,$art){
	$dizi[0][0]=0;
	$say=0;
	for($i=0;$i<360;$i+=$art){
		
		$y = $r * sin(deg2rad($i));
		
		$x = $r * cos(deg2rad($i));

		$dizi[0][$say]=$x+$merkez;
		$dizi[1][$say]=$y+$merkez;
		$say++;
	}
	return $dizi;
}

function ArCode($Id){
	$splitId=str_split($Id);
	$temp="";
	foreach($splitId as $number){
	$temp.=addZero(decbin(intval($number)));
	}
	return $temp."1111";
}

function addZero($number){
	$temp=$number;
		
		if(strlen($temp) ==1){
		$temp="000".$temp;
		}
		elseif(strlen($temp) ==2){
		$temp="00".$temp;
		}
		elseif(strlen($temp) ==3){
		$temp="0".$temp;
		}

	return $temp;
}

?>