<?php

header('Content-type: image/svg+xml');
error_reporting(0);

$rKodR=$_GET["r"] ? $_GET["r"] : 0;
$rKodG=$_GET["g"] ? $_GET["g"] : 0;
$rKodB=$_GET["b"] ? $_GET["b"] : 0;
$getCode=$_GET["c"] ? $_GET["c"] : rand(0,100000);
$tGet=$_GET["s"] ? $_GET["s"] : 1;

$times=$tGet;

$tuvalX=430*$times;
$tuvalY=$tuvalX;



?>
<svg width="<?php echo $tuvalX; ?>" height="<?php echo $tuvalY; ?>" viewBox="0 0 <?php echo $tuvalX.' '.$tuvalY; ?>" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" >
   

<?php


echo '<circle cx="'.($tuvalX/2).'" cy="'.($tuvalY/2).'" r="'.(208*$times).'" style="fill:rgb('.$rKodR.','.$rKodB.','.$rKodG.');" />';
echo '<circle cx="'.($tuvalX/2).'" cy="'.($tuvalY/2).'" r="'.(200*$times).'" style="fill:rgb(255,255,255);" />';
echo '<circle cx="'.($tuvalX/2).'" cy="'.($tuvalY/2).'" r="'.(113*$times).'" style="fill:rgb('.$rKodR.','.$rKodB.','.$rKodG.');" />';
echo '<circle cx="'.($tuvalX/2).'" cy="'.($tuvalY/2).'" r="'.(65*$times).'" style="fill:rgb(255,255,255);" />';


$noktab=Daire(89*$times,$tuvalY/2,90);
for($i=0;$i<4;$i++){
	if($i!=1)
		echo '<circle cx="'.($noktab[0][$i]).'" cy="'.($noktab[1][$i]).'" r="'.(20*$times).'" style="fill:rgb(255,255,255);" />';
}

$Code = ArCode($getCode);

$CodeSay=0;
$nokta=Daire(180*$times,$tuvalY/2,9);
$nokta2=Daire(112*$times,$tuvalY/2,9);
$nokta3=Daire(((180-112)/2+112)*$times,$tuvalY/2,9);
for($i=0;$i<40;$i++){
	
	if(strlen($Code)>$CodeSay){
		if($Code[$CodeSay]==1){
			echo '<circle cx="'.($nokta[0][$i]).'" cy="'.($nokta[1][$i]).'" r="'.(15*$times).'" style="fill:rgb('.$rKodR.','.$rKodB.','.$rKodG.');" />';
			echo '<line x1="'.($nokta[0][$i]).'" y1="'.($nokta[1][$i]).'" x2="'.($nokta2[0][$i]).'" y2="'.($nokta2[1][$i]).'" style="stroke:rgb('.$rKodR.','.$rKodB.','.$rKodG.');stroke-width:'.(30*$times).'" />"';
			//echo '<circle cx="'.($nokta[0][$i]).'" cy="'.($nokta[1][$i]).'" r="'.(13*$times).'" style="fill:rgb('.$rKodR.','.$rKodB.','.$rKodG.');" />';
		}
		//else echo '<rect x="'.($nokta3[0][$i]-(14*$times)).'" y="'.($nokta3[1][$i]-(14*$times)).'" width="'.(28*$times).'" height="'.(28*$times).'" style="fill:rgb('.$rKodR.','.$rKodB.','.$rKodG.');"  />';
		//else echo '<text x="'.($nokta3[0][$i]-(18*$times)).'" y="'.($nokta3[1][$i]+(18*$times)).'" style="fill:rgb('.$rKodR.','.$rKodB.','.$rKodG.');font-size:'.(50*$times).'px;" >✦</text>';
		$CodeSay++;
	}
	else {
		if(rand (0,1)==1){
			echo '<circle cx="'.($nokta[0][$i]).'" cy="'.($nokta[1][$i]).'" r="'.(15*$times).'" style="fill:rgb('.$rKodR.','.$rKodB.','.$rKodG.');" />';
			echo '<line x1="'.($nokta[0][$i]).'" y1="'.($nokta[1][$i]).'" x2="'.($nokta2[0][$i]).'" y2="'.($nokta2[1][$i]).'" style="stroke:rgb('.$rKodR.','.$rKodB.','.$rKodG.');stroke-width:'.(30*$times).'" />"';
			//echo '<circle cx="'.($nokta[0][$i]).'" cy="'.($nokta[1][$i]).'" r="'.(13*$times).'" style="fill:rgb('.$rKodR.','.$rKodB.','.$rKodG.');" />';
		}
			
		//else echo '<rect x="'.($nokta3[0][$i]-(14*$times)).'" y="'.($nokta3[1][$i]-(14*$times)).'" width="'.(28*$times).'" height="'.(28*$times).'" style="fill:rgb('.$rKodR.','.$rKodB.','.$rKodG.');"  />';
		//else echo '<text x="'.($nokta3[0][$i]-(18*$times)).'" y="'.($nokta3[1][$i]+(18*$times)).'" style="fill:rgb('.$rKodR.','.$rKodB.','.$rKodG.');font-size:'.(50*$times).'px;" >✦</text>';
	}
	//echo '<circle cx="'.($nokta3[0][$i]).'" cy="'.($nokta3[1][$i]).'" r="'.(6*$times).'" style="fill:rgb(0,0,0);" />';
}

?>
</svg>

<?php

function Daire($r,$merkez,$art){
	$dizi[0][0]=0;
	$say=0;
	for($i=0;$i<360;$i+=$art){
		
		$y = $r * sin(deg2rad($i));
		
		$x = $r * cos(deg2rad($i));

		$dizi[0][$say]=$x+$merkez;
		$dizi[1][$say]=$y+$merkez;
		$say++;
	}
	return $dizi;
}

function ArCode($Id){
	$splitId=str_split($Id);
	$temp="";
	foreach($splitId as $number){
	$temp.=addZero(decbin(intval($number)));
	}
	return $temp."1111";
}

function addZero($number){
	$temp=$number;
		
		if(strlen($temp) ==1){
		$temp="000".$temp;
		}
		elseif(strlen($temp) ==2){
		$temp="00".$temp;
		}
		elseif(strlen($temp) ==3){
		$temp="0".$temp;
		}

	return $temp;
}

?><?php

header('Content-type: image/svg+xml');
error_reporting(0);

$rKodR=$_GET["r"] ? $_GET["r"] : 0;
$rKodG=$_GET["g"] ? $_GET["g"] : 0;
$rKodB=$_GET["b"] ? $_GET["b"] : 0;
$getCode=$_GET["c"] ? $_GET["c"] : rand(0,100000);
$tGet=$_GET["s"] ? $_GET["s"] : 1;

$times=$tGet;

$tuvalX=430*$times;
$tuvalY=$tuvalX;



?>
<svg width="<?php echo $tuvalX; ?>" height="<?php echo $tuvalY; ?>" viewBox="0 0 <?php echo $tuvalX.' '.$tuvalY; ?>" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" >
   

<?php


echo '<circle cx="'.($tuvalX/2).'" cy="'.($tuvalY/2).'" r="'.(208*$times).'" style="fill:rgb('.$rKodR.','.$rKodB.','.$rKodG.');" />';
echo '<circle cx="'.($tuvalX/2).'" cy="'.($tuvalY/2).'" r="'.(200*$times).'" style="fill:rgb(255,255,255);" />';
echo '<circle cx="'.($tuvalX/2).'" cy="'.($tuvalY/2).'" r="'.(113*$times).'" style="fill:rgb('.$rKodR.','.$rKodB.','.$rKodG.');" />';
echo '<circle cx="'.($tuvalX/2).'" cy="'.($tuvalY/2).'" r="'.(65*$times).'" style="fill:rgb(255,255,255);" />';


$noktab=Daire(89*$times,$tuvalY/2,90);
for($i=0;$i<4;$i++){
	if($i!=1)
		echo '<circle cx="'.($noktab[0][$i]).'" cy="'.($noktab[1][$i]).'" r="'.(20*$times).'" style="fill:rgb(255,255,255);" />';
}

$Code = ArCode($getCode);

$CodeSay=0;
$nokta=Daire(180*$times,$tuvalY/2,9);
$nokta2=Daire(112*$times,$tuvalY/2,9);
$nokta3=Daire(((180-112)/2+112)*$times,$tuvalY/2,9);
for($i=0;$i<40;$i++){
	
	if(strlen($Code)>$CodeSay){
		if($Code[$CodeSay]==1){
			echo '<circle cx="'.($nokta[0][$i]).'" cy="'.($nokta[1][$i]).'" r="'.(15*$times).'" style="fill:rgb('.$rKodR.','.$rKodB.','.$rKodG.');" />';
			echo '<line x1="'.($nokta[0][$i]).'" y1="'.($nokta[1][$i]).'" x2="'.($nokta2[0][$i]).'" y2="'.($nokta2[1][$i]).'" style="stroke:rgb('.$rKodR.','.$rKodB.','.$rKodG.');stroke-width:'.(30*$times).'" />"';
			//echo '<circle cx="'.($nokta[0][$i]).'" cy="'.($nokta[1][$i]).'" r="'.(13*$times).'" style="fill:rgb('.$rKodR.','.$rKodB.','.$rKodG.');" />';
		}
		//else echo '<rect x="'.($nokta3[0][$i]-(14*$times)).'" y="'.($nokta3[1][$i]-(14*$times)).'" width="'.(28*$times).'" height="'.(28*$times).'" style="fill:rgb('.$rKodR.','.$rKodB.','.$rKodG.');"  />';
		//else echo '<text x="'.($nokta3[0][$i]-(18*$times)).'" y="'.($nokta3[1][$i]+(18*$times)).'" style="fill:rgb('.$rKodR.','.$rKodB.','.$rKodG.');font-size:'.(50*$times).'px;" >✦</text>';
		$CodeSay++;
	}
	else {
		if(rand (0,1)==1){
			echo '<circle cx="'.($nokta[0][$i]).'" cy="'.($nokta[1][$i]).'" r="'.(15*$times).'" style="fill:rgb('.$rKodR.','.$rKodB.','.$rKodG.');" />';
			echo '<line x1="'.($nokta[0][$i]).'" y1="'.($nokta[1][$i]).'" x2="'.($nokta2[0][$i]).'" y2="'.($nokta2[1][$i]).'" style="stroke:rgb('.$rKodR.','.$rKodB.','.$rKodG.');stroke-width:'.(30*$times).'" />"';
			//echo '<circle cx="'.($nokta[0][$i]).'" cy="'.($nokta[1][$i]).'" r="'.(13*$times).'" style="fill:rgb('.$rKodR.','.$rKodB.','.$rKodG.');" />';
		}
			
		//else echo '<rect x="'.($nokta3[0][$i]-(14*$times)).'" y="'.($nokta3[1][$i]-(14*$times)).'" width="'.(28*$times).'" height="'.(28*$times).'" style="fill:rgb('.$rKodR.','.$rKodB.','.$rKodG.');"  />';
		//else echo '<text x="'.($nokta3[0][$i]-(18*$times)).'" y="'.($nokta3[1][$i]+(18*$times)).'" style="fill:rgb('.$rKodR.','.$rKodB.','.$rKodG.');font-size:'.(50*$times).'px;" >✦</text>';
	}
	//echo '<circle cx="'.($nokta3[0][$i]).'" cy="'.($nokta3[1][$i]).'" r="'.(6*$times).'" style="fill:rgb(0,0,0);" />';
}

?>
</svg>

<?php

function Daire($r,$merkez,$art){
	$dizi[0][0]=0;
	$say=0;
	for($i=0;$i<360;$i+=$art){
		
		$y = $r * sin(deg2rad($i));
		
		$x = $r * cos(deg2rad($i));

		$dizi[0][$say]=$x+$merkez;
		$dizi[1][$say]=$y+$merkez;
		$say++;
	}
	return $dizi;
}

function ArCode($Id){
	$splitId=str_split($Id);
	$temp="";
	foreach($splitId as $number){
	$temp.=addZero(decbin(intval($number)));
	}
	return $temp."1111";
}

function addZero($number){
	$temp=$number;
		
		if(strlen($temp) ==1){
		$temp="000".$temp;
		}
		elseif(strlen($temp) ==2){
		$temp="00".$temp;
		}
		elseif(strlen($temp) ==3){
		$temp="0".$temp;
		}

	return $temp;
}

?>