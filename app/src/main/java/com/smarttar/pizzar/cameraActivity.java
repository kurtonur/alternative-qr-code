package com.smarttar.pizzar;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.core.Rect;
import org.opencv.imgproc.Imgproc;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.smarttar.pizzar.lib.Scan;
import com.smarttar.pizzar.lib.myCamera;

public class cameraActivity extends Activity implements CvCameraViewListener2 {

    // Used for logging success or failure messages
    private static final String TAG = "OCVSample::Activity";


    private myCamera mOpenCvCameraView;
    private Button buttonR;
    private ImageView FoundImg;
    private TextView codetext;
    Intent drawactivity;
    // These variables are used (at the moment) to fix camera orientation from 270degree to 0degree
    Mat mRgba;
    Mat mRgbaF;
    Mat mRgbaT;
    Mat mRgbaCrop;
    Rect cropArea;

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.i(TAG, "OpenCV loaded successfully");
                    mOpenCvCameraView.enableView();
                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };

    public cameraActivity() {
        Log.i(TAG, "Instantiated new " + this.getClass());
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.cameraactivitylayout);

        mOpenCvCameraView = (myCamera) findViewById(R.id.opencvcamera);

        drawactivity = new Intent(this, drawActivity.class);

        buttonR = (Button) findViewById(R.id.buttonr);
        buttonR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Scan.Clear();
                codetext.setText("");
                FoundImg.setImageResource(R.drawable.empty);
            }
        });

        FoundImg= (ImageView) findViewById(R.id.isfound);
        codetext= (TextView) findViewById(R.id.text);
        cropArea = new Rect(220,220,270,270);

        mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
        mOpenCvCameraView.setCvCameraViewListener(this);
        mOpenCvCameraView.setMaxFrameSize(720, 1280);
        mOpenCvCameraView.setScaleX(1.75f);
        mOpenCvCameraView.setScaleY(1.75f);



        new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {
                if(Scan.isFound) {
                    Bitmap b = Bitmap.createBitmap(mRgbaCrop.rows(), mRgbaCrop.cols(), Bitmap.Config.ARGB_8888);
                    Utils.matToBitmap(mRgbaCrop, b, true);
                    FoundImg.setImageBitmap(b);
                    codetext.setText(Scan.code);

                }
            }

            public void onFinish() {
               this.start();
            }
        }.start();

    }

    @Override
    public void onPause()
    {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        if (!OpenCVLoader.initDebug()) {
            Log.d(TAG, "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, mLoaderCallback);
        } else {
            Log.d(TAG, "OpenCV library found inside package. Using it!");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    public void onCameraViewStarted(int width, int height) {

        mRgba = new Mat(height, width, CvType.CV_8UC4);
        mRgbaF = new Mat(height, width, CvType.CV_8UC4);
        mRgbaT = new Mat(width, width, CvType.CV_8UC4);
        Scan.createCodeList();
        Scan.Clear();
        Scan.isShowTest = true;
    }

    public void onCameraViewStopped() {
        mRgba.release();
        mRgbaF.release();
        mRgbaT.release();
        mRgbaCrop.release();
    }

    public Mat onCameraFrame(CvCameraViewFrame inputFrame) {
        mRgba = inputFrame.rgba();
        Core.transpose(mRgba, mRgbaT);
        Imgproc.resize(mRgbaT, mRgbaF, mRgbaF.size(), 0,0, 0);
        Core.flip(mRgbaF, mRgba, 1 );
            try {
                if(!Scan.isFound){
                    //mRgbaCrop = new Mat(mRgba,cropArea);
                    mRgbaCrop = new Mat(mRgba,cropArea).clone();
                    Scan.Camera(mRgbaCrop);
                }
                else{
                    //New Activity
                    //startActivity(drawactivity);
                }
            }
            catch(Exception error) {
                Log.d(TAG, error.getMessage());
            }
        return mRgba;
    }

}

