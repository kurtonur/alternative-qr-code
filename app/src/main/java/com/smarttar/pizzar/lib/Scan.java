package com.smarttar.pizzar.lib;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.List;

public class Scan
{
    public static boolean isFound= false;
    public static List<String> codeList;
    public static String code = "";
    public static boolean isShowTest = false;

    public static void Camera(Mat img){
        Core.normalize(img,img,0,255,Core.NORM_MINMAX);
        //Imgproc.threshold( img, img, 175,255,Imgproc.THRESH_BINARY);
        Imgproc.cvtColor (img, img, Imgproc.COLOR_RGBA2GRAY);
        outsideBorder(img);

    }

    private static void outsideBorder(Mat img){

        Mat circles = new Mat();
        //121-123
        Imgproc.HoughCircles (img, circles, Imgproc.CV_HOUGH_GRADIENT, 2, 110, 100, 100,  40,130);

        Point pt = new Point();
        if(circles.cols()>0) {

            double[] data = circles.get(0, 0);
            pt.x = data[0];
            pt.y = data[1];
            double rho = data[2];

            if ((img.rows() / 2 + 10 > pt.x && img.rows() / 2 - 10 < pt.x) && (img.cols() / 2 + 10 > pt.y && img.cols() / 2 - 10 < pt.y)) {

                insideBorder(img, new Circle(pt, rho));

            }

        }
    }

    private static void insideBorder(Mat img,Circle outsideBorder){

        Mat circles = new Mat ();
        //48-52
        Imgproc.HoughCircles (img, circles, Imgproc.CV_HOUGH_GRADIENT, 2, 110, 100, 100, (int)(outsideBorder.r/3), (int)(outsideBorder.r/3+10));

        Point pt = new Point ();

        for (int i = 0; i < circles.cols (); i++) {
            double[] data = circles.get (0, i);
            pt.x = data [0];
            pt.y = data [1];
            double rho = data [2];

            if((img.rows()/2+10>pt.x && img.rows()/2-10<pt.x) && (img.cols()/2+10>pt.y && img.cols()/2-10<pt.y)){

                calibirationPoints(img,outsideBorder,new Circle(pt,rho));

            }
        }
    }

    private static void calibirationPoints(Mat img,Circle outsideBorder,Circle insideBorder){


        Mat circles = new Mat();

        Imgproc.HoughCircles (img, circles, Imgproc.CV_HOUGH_GRADIENT, 2, 1, 100, 20, (int)(insideBorder.r/8)-2 , (int)(insideBorder.r/8)+1);

        Point pt = new Point ();
        List<Circle> calibirationPoints= new ArrayList<Circle>();
        Circle upCalibirationPoint= new Circle();
        int countPoint=0;

        for (int i = 0; i < circles.cols (); i++) {
            double[] data = circles.get (0, i);
            pt.x = data [0];
            pt.y = data [1];
            double rho = data [2];
            double distanceCenter = Distance(insideBorder.Center,pt);
            if(distanceCenter>insideBorder.r && distanceCenter<insideBorder.r+(outsideBorder.r-insideBorder.r)/4){

                countPoint++;
                calibirationPoints.add(new Circle(new Point(data [0],data [1]),rho));

                if(countPoint == 3){
                    if(Angle(insideBorder.Center, calibirationPoints.get(0).Center,calibirationPoints.get(1).Center)<=120){
                        if(Angle(insideBorder.Center,calibirationPoints.get(0).Center,calibirationPoints.get(2).Center)>=160){
                            upCalibirationPoint = calibirationPoints.get(1);
                        }
                        else{
                            upCalibirationPoint = calibirationPoints.get(0);
                        }
                    }
                    else if(Angle(insideBorder.Center, calibirationPoints.get(0).Center, calibirationPoints.get(2).Center)<=120){
                        upCalibirationPoint = calibirationPoints.get(2);
                    }
                    break;
                }

            }
        }

        if(countPoint == 3)
            replaceRotation(img,outsideBorder,insideBorder,calibirationPoints,upCalibirationPoint);

    }

    private static void replaceRotation(Mat img,Circle outsideBorder,Circle insideBorder,List<Circle> calibirationPoints,Circle upCalibirationPoint){

        double angle=Angle(insideBorder.Center,upCalibirationPoint.Center,new Point(insideBorder.Center.x,insideBorder.Center.y-insideBorder.Center.y));
        rotateCode(img,insideBorder.Center,angle);
        if(angle !=0)
            calibirationPoints(img,outsideBorder,insideBorder);
        else{
            codeCircles(img,outsideBorder,insideBorder,calibirationPoints);
        }

    }

    private static void codeCircles(Mat img,Circle outsideBorder,Circle insideBorder,List<Circle> calibirationPoints){

        Mat circles = new Mat ();
        //6-8
        Imgproc.HoughCircles (img, circles, Imgproc.CV_HOUGH_GRADIENT, 2, 1, 100, 20, (int)(insideBorder.r/8)-2 , (int)(insideBorder.r/8)+2);

        Point pt = new Point ();
        List<Point> codePointsFirst= new ArrayList<Point>();
        List<Point> codePointsSecond= new ArrayList<Point>();

        for (int i = 0; i < circles.cols (); i++) {
            double[] data = circles.get (0, i);
            pt.x = data [0];
            pt.y = data [1];
            double rho = data [2];
            double distanceCenter = Distance(insideBorder.Center,pt);

            if(distanceCenter>insideBorder.r+(outsideBorder.r-insideBorder.r)/4 && distanceCenter<insideBorder.r+(outsideBorder.r-insideBorder.r)*7/10){

                codePointsFirst.add(new Point(pt.x,pt.y));
            }
            else if(distanceCenter>insideBorder.r+(outsideBorder.r-insideBorder.r)*7/10 && distanceCenter<outsideBorder.r){

                codePointsSecond.add(new Point(pt.x,pt.y));
            }

        }

        List<Point> firstCircle =  createCirclePoints(insideBorder.Center,insideBorder.r+(outsideBorder.r-insideBorder.r)*5/10,18);
        List<Point> SecoundCircle = createCirclePoints(insideBorder.Center,insideBorder.r+(outsideBorder.r-insideBorder.r)*9/10,15);
        frequencyOfCode(returnCode(firstCircle,codePointsFirst,SecoundCircle,codePointsSecond,(outsideBorder.r-insideBorder.r)/7),codeList,2,2);

        ShowTest(img,outsideBorder,insideBorder,calibirationPoints,codePointsFirst,codePointsSecond);

    }

    private static String returnCode(List<Point> angularPoints1,List<Point> codePoints1,List<Point> angularPoints2,List<Point> codePoints2,double rectangleSize){

        String WholeCode="";
        String BinaryCode="";
        boolean addable=true;
        int count=0;
        for(Point temp  : angularPoints1 ){

            for (Point Codes : codePoints1){
                if((temp.x-rectangleSize<Codes.x && temp.x+rectangleSize>Codes.x) && (temp.y-rectangleSize<Codes.y && temp.y+rectangleSize>Codes.y)){
                    BinaryCode+="1";
                    addable= false;
                    count++;
                    break;
                }
            }
            if(addable){
                BinaryCode += "0";
                count++;
            }

            if(count == 4){
                if(!BinaryCode.equals("1111")){
                    WholeCode+=String.valueOf(Integer.parseInt(BinaryCode,2));
                    BinaryCode="";
                    count=0;

                }
                else return WholeCode;
            }
            addable=true;
        }
        BinaryCode="";
        addable=true;
        count=0;

        for(Point temp  : angularPoints2 ){

            for (Point Codes : codePoints2){
                if((temp.x-rectangleSize<Codes.x && temp.x+rectangleSize>Codes.x) && (temp.y-rectangleSize<Codes.y && temp.y+rectangleSize>Codes.y)){
                    BinaryCode+="1";
                    addable= false;
                    count++;
                    break;
                }
            }
            if(addable){
                BinaryCode += "0";
                count++;
            }

            if(count == 4){
                if(!BinaryCode.equals("1111")){
                    WholeCode+=String.valueOf(Integer.parseInt(BinaryCode,2));
                    BinaryCode="";
                    count=0;

                }
                else return WholeCode;
            }
            addable=true;
        }
        return WholeCode;

    }

    private static void frequencyOfCode(String data,List<String> array,int arraySize,int frequency){

        if(array.size() == arraySize){
            int Frequency=0;
            code=codeList.get(0);
            for(String Code1 : array){
                int CcountF=0;
                for(String Code2 : array){
                    if(Code1.equals(Code2)){
                        CcountF++;
                    }
                }
                if(Frequency<CcountF){
                    Frequency=CcountF;
                    code=Code1;
                }
            }
            if(Frequency >=frequency){
                isFound=true;
            }
            else Clear();
        }
        else{
            array.add(data);
        }

    }

    public static void createCodeList(){

        codeList = new ArrayList<String>();

    }

    public static void Clear(){

        isFound=false;
        codeList.clear();
        code="";

    }

    private static List<Point> createCirclePoints(Point Center,double r,int art){

        List<Point> daireList= new ArrayList<Point>();
        for(int i=0;i<360;i+=art){

            double x = r * Math.cos(Math.PI * i / 180.0)+Center.x;
            double y = r * Math.sin(Math.PI * i / 180.0)+Center.y;

            daireList.add(new Point(x,y));
        }
        return daireList;

    }

    private static void rotateCode(Mat img,Point Center,double angle){

        if(angle != 0){
            Point src_center = new Point(Center.x, Center.y);
            Mat rot_mat = Imgproc.getRotationMatrix2D(src_center, angle, 1.0);
            Imgproc.warpAffine(img, img, rot_mat, img.size());
        }

    }

    private static double Distance(Point Center, Point point){

        return Math.sqrt(Math.pow(Center.x-point.x,2)+Math.pow(Center.y-point.y,2));

    }

    private static double Angle(Point Center, Point point1,Point point2){

        double P1distance = Distance(Center,point1);
        double P2distance = Distance(Center,point2);
        double P1andP2distance = Distance(point1,point2);
        double AngleCos = (Math.pow(P1distance,2)+Math.pow(P2distance,2)-Math.pow(P1andP2distance,2))/(2*P1distance*P2distance);
        return Math.acos(AngleCos) * (180 / Math.PI);

    }

    private static void ShowTest(Mat img,Circle outsideBorder,Circle insideBorder,List<Circle> calibirationPoints,List<Point> firstCircle,List<Point> SecoundCircle){
        if(isShowTest){

            Imgproc.cvtColor (img, img, Imgproc.COLOR_GRAY2RGB);

            for(Point pd : firstCircle){
                Imgproc.circle (img, pd, 1, new Scalar (0, 255, 0, 255), 5);
                //Imgproc.rectangle (img, new Point(pd.x-(outsideBorder.r-insideBorder.r)/7,pd.y-(outsideBorder.r-insideBorder.r)/7),new Point(pd.x+(outsideBorder.r-insideBorder.r)/7,pd.y+(outsideBorder.r-insideBorder.r)/7) , new Scalar (0, 255, 0, 255),1);
            }
            for(Point pd : SecoundCircle){
                Imgproc.circle (img, pd, 1, new Scalar (255, 0, 0, 255), 5);
                //Imgproc.rectangle (img, new Point(pd.x-(outsideBorder.r-insideBorder.r)/7,pd.y-(outsideBorder.r-insideBorder.r)/7),new Point(pd.x+(outsideBorder.r-insideBorder.r)/7,pd.y+(outsideBorder.r-insideBorder.r)/7) , new Scalar (0, 255, 0, 255),1);
            }

            Imgproc.circle(img,outsideBorder.Center,(int)outsideBorder.r,new Scalar(255,255,0),3);
            Imgproc.circle(img,insideBorder.Center,(int)insideBorder.r,new Scalar(0,255,255),2);
            Imgproc.circle(img,calibirationPoints.get(0).Center,(int)calibirationPoints.get(0).r,new Scalar(255,0,0),2);
            Imgproc.circle(img,calibirationPoints.get(1).Center,(int)calibirationPoints.get(1).r,new Scalar(0,255,0),2);
            Imgproc.circle(img,calibirationPoints.get(2).Center,(int)calibirationPoints.get(2).r,new Scalar(0,0,255),2);
        }
    }

}
