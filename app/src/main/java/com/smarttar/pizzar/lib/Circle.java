package com.smarttar.pizzar.lib;

import org.opencv.core.Point;

public class Circle
{
    public Point Center;
    public double r;

    public Circle(Point Center, double r){
        this.Center=Center;
        this.r=r;
    }

    public Circle(){
        this.Center=new Point(0,0);
        this.r=0;
    }

}


