package com.smarttar.pizzar;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

import com.smarttar.pizzar.lib.Scan;

public class drawActivity extends Activity {

    private TextView text;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawactivitylayout);
        text = (TextView) findViewById(R.id.text);
        text.setText(Scan.code);
    }

}
