# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is alternative qr code project.
* Circle qr code project

### What do you need to run it? ###

* Android Studio
* localhost Server for PHP to create marker (Image)
* OpenCV Manager apk (OpenCV_apk folder)
* Of course, an android phone

### Marker Creator ###

* You should run marker.php on your Apache Server (marker creator)
* Then add "?r=0&b=0&g=0&s=1&c=123456" to end of link
* r = Red
* b = blue
* g = green
* s = size
* c = Code (you will see it when you scan)

### Android Studio ###

* Run cameraActivity.
* Also, if it doesn't work, you have to try to install OpenCV Manager apk

### Screenshots ###

 ![Screenshot1](https://bitbucket.org/kurtonur/alternative-qr-code/raw/b1988f1df78a9614b1f07c5d6a7ca30854740192/Images/Screenshot_20201222-024805.png) 
 ![Screenshot2](https://bitbucket.org/kurtonur/alternative-qr-code/raw/b1988f1df78a9614b1f07c5d6a7ca30854740192/Images/Screenshot_20201222-024834.png) 
 ![Screenshot3](https://bitbucket.org/kurtonur/alternative-qr-code/raw/b1988f1df78a9614b1f07c5d6a7ca30854740192/Images/Screenshot_20201222-025022.png) 
 ![Screenshot4](https://bitbucket.org/kurtonur/alternative-qr-code/raw/b1988f1df78a9614b1f07c5d6a7ca30854740192/Images/Screenshot_20201222-025028.png) 

### Note ###

* This project doesn't work perfectly right now. I am still working on it.